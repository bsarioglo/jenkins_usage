## WTF?

This is repo for a Jenkins usage. You can find several branches for a different jobs with Jenkinsfiles inside.

### How to use?

First of all you need to install Jenkins. You can use ansible and vagrant provision to meet your need.
I maid own "flow" but it's awful and far away from best pratice, but I already spend to much time to this task, so.
Maybe I'll improve it later! :) [JENKINS INSTALLATION](https://gitlab.com/bsarioglo/jenkins/)

### Varaibles

You'll need to add sveral ENV variables to proceed and hide our sensetive (nah) data.

```staging``` - IP address of your staging server

```production``` - IP address of your production server

```sshuser``` - SSH user to access servers. More likely it will be ```jenkins```

```gitrepo``` - SSH link to your Git repo.

***You can add ssh key variable and edit Jenkinsfiles if you wish. But there's not much sense to do so.***
